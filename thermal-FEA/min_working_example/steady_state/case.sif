Header
  CHECK KEYWORDS Warn
  Mesh DB "." "."
  Include Path ""
  Results Directory ""
End

Simulation
  Max Output Level = 5
  Coordinate System = Cartesian
  Simulation Type = Steady state
  Steady State Max Iterations = 1
  Output Intervals(1) = 1
  Solver Input File = case.sif
  Post File = case.vtu
Save Geometry Ids = Logical True
End

Constants
  Gravity(4) = 0 -1 0 9.82
  Stefan Boltzmann = 5.670374419e-08
  Permittivity of Vacuum = 8.85418781e-12
  Permeability of Vacuum = 1.25663706e-6
  Boltzmann Constant = 1.380649e-23
  Unit Charge = 1.6021766e-19
End

Body 1
  Target Bodies(1) = 1
  Name = "Body 1"
  Equation = 1
  Material = 1
  Initial condition = 1
End

Body 2
  Target Bodies(1) = 2
  Name = "Body 2"
  Equation = 1
  Material = 1
  Initial condition = 1
End

Body 3
  Target Bodies(1) = 3
  Name = "Body Property 3"
  Equation = 1
  Material = 1
  Initial condition = 1
End

Body 4
  Target Bodies(1) = 4
  Name = "Body 4"
  Equation = 1
  Material = 1
  Initial condition = 1
End

Body 5
  Target Bodies(1) = 5
  Name = "Body 5"
  Equation = 1
  Material = 1
  Initial condition = 1
End

Solver 1
  Equation = Heat Equation
  Variable = Temperature
  Calculate Loads = True
  Procedure = "HeatSolve" "HeatSolver"
  Exec Solver = Always
  Stabilize = True
  Optimize Bandwidth = True
  Steady State Convergence Tolerance = 1.0e-5
  Nonlinear System Convergence Tolerance = 1.0e-7
  Nonlinear System Max Iterations = 20
  Nonlinear System Newton After Iterations = 3
  Nonlinear System Newton After Tolerance = 1.0e-4
  Nonlinear System Relaxation Factor = 0.6
  Linear System Solver = Iterative
  Linear System Iterative Method = BiCGStab
  Linear System Max Iterations = 500
  Linear System Convergence Tolerance = 1.0e-10
  BiCGstabl polynomial degree = 2
  Linear System Preconditioning = ILU3
  Linear System ILUT Tolerance = 1.0e-3
  Linear System Abort Not Converged = False
  Linear System Residual Output = 10
  Linear System Precondition Recompute = 1
End

Equation 1
  Name = "Heat"
  Active Solvers(1) = 1
End

Material 1
  Name = "Aluminium (generic)"
  Poisson ratio = 0.35
  Heat Capacity = 897.0
  Sound speed = 5000.0
  Heat expansion Coefficient = 23.1e-6
  Heat Conductivity = 237.0
  Density = 2700.0
  Youngs modulus = 70.0e9
End

Material 2
  Name = "Polyvinyl chloride (generic)"
  Poisson ratio = 0.41
  Emissivity = 0.0
  Density = 1380.0
  Youngs modulus = 3100.0e6
  Heat Conductivity = 0.16
  Heat Capacity = 900.0
  Heat expansion Coefficient = 80.0e-6
End

Initial Condition 1
  Name = "Init temp"
  Temperature = 300
End

Boundary Condition 1
  Target Boundaries(5) = 6 14 15 17 25 
  Name = "Radiation_Gray"
  Emissivity = 0.6
  Radiation Boundary Open = True
  Radiation = Diffuse Gray
  External Temperature = 15
End

Boundary Condition 2
  Target Boundaries(1) = 2 
  Name = "Sun_Q"
  Heat Flux = 1400.0
  External Temperature = 15.0
  Emissivity = 0.6
  Radiation = Idealized
End

Boundary Condition 3
  Target Boundaries(3) = 13 18 29 
  Name = "Idealized_rad"
  External Temperature = 15
  Emissivity = 0.6
  Radiation = Idealized
End

Boundary Condition 4
  Target Boundaries(1) = 16 
  Name = "pcb_heating"
  Radiation Boundary Open = True
  Emissivity = 0.6
  External Temperature = 15
  Radiation = Diffuse Gray
  Heat Flux = 1100.0
End
