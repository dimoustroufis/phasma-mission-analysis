#ifndef FINESUNSENSOR_H
#define FINESUNSENSOR_H

#include "architecture/_GeneralModuleFiles/sys_model.h"
#include "architecture/utilities/bskLogging.h"
#include "architecture/messaging/messaging.h"
#include "msgPayloadDefCpp/FSSRawDataMsgPayload.h"
#include "architecture/msgPayloadDefC/EclipseMsgPayload.h"
#include "architecture/msgPayloadDefC/SpicePlanetStateMsgPayload.h"
#include "architecture/msgPayloadDefC/SCStatesMsgPayload.h"
#include <eigen3/Eigen/Core>
#include <random>


/**
 * @brief Models a Fine Sun Sensor
 * 
 */
class FineSunSensor: public SysModel{
    private:
        double fov; /* Sensor FOV in rad from the center of the boresight to the edge of the FOV */
        double noise1Sigma; /* The 3-sigma half-cone anglular uncertainty of the sensor */
        bool isValid; /* Flag to indicate whether the sensor output is valid */
        double sunVisibilityThreshold; /* Threshold to consider a fully obscured Sun */
        Eigen::Vector3d sHat_B; /* Sun heading unit vector in the body frame */

        // The sun illumination is considered to be equal along a spacecraft side, so the position
        // of the FSS in the spacecraft body frame is not required - only its attitude is.
        Eigen::Matrix3d dcm_BFSS; /* Rotation matrix from the FSS frame to the body frame */
        Eigen::Vector3d nHat_FSS; /* Boresight heading in the sensor frame. It is assumed that the boresight is alligned with one of the sensor frame axes. */
        Eigen::Vector3d nHat_B; /* Boresight heading in the body frame */

        // Variables to store input message contents
        SCStatesMsgPayload stateCurrent; /* Current spacecraft state info */
        EclipseMsgPayload sunVisibilityFactor; /* Sun visibility factor 0 means the sun is fully hidden and 1 means it is fully visible */
        SpicePlanetStateMsgPayload sunData; /* Sun ephemeris data */

        // Variables related to the noise distribution
        std::mt19937 random_engine;
        std::normal_distribution<double> distribution; /* Sensor noise distribution */

    public:
        FineSunSensor();
        ~FineSunSensor();
        
        // Basilisk simulation functions
        void Reset(uint64_t current_sim_nanos);
        void UpdateState(uint64_t current_sim_nanos);
        void readInputMessages();
        void writeOutputMessage(uint64_t current_sim_nanos);

        // Operational functions
        void findSunVector(); /* Determines the sun heading vector in the body frame */
        void limitOutput(); /* Considers eclipse cases events and out-of-fov sun positions */
        void addSensorErrors(); /* Applies the various sensor errors to the output */

        // Setter functions
        void setFOV(double _fov);
        void setNoise1Sigma(double _noise1Sigma);
        void setSunVisibilityThreshold(double _sunVisibilityThreshold);
        void setSensorFrame(Eigen::Matrix3d _dcm_BFSS);
        void setBoresightHeading(Eigen::Vector3d _nHat_FSS);

        Message<FSSRawDataMsgPayload> fssDataOutMsg; /* Output message */
        ReadFunctor<EclipseMsgPayload> eclipseInMsg; /* Input message for eclipse data */
        ReadFunctor<SpicePlanetStateMsgPayload> sunInMsg; /* Input message for sun data */
        ReadFunctor<SCStatesMsgPayload> stateInMsg; /* input message for spacecraft state */
        BSKLogger bskLogger; /* BSK Logging */
};

#endif
