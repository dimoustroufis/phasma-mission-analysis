#include "magFSSEKF.h"
#include <math.h>
#include <iostream>
#include "architecture/utilities/avsEigenSupport.h"
#include "architecture/utilities/macroDefinitions.h"
#include "architecture/utilities/rigidBodyKinematics.h"

MagFSSEKF::MagFSSEKF(){
    // Zero all covariance matrices - These must be set using the provided method
    this->covarMag.setZero();
    this->covarFSS.setZero();
    this->covarState.setZero();

    // Set all full quaternion members to unit quaternions
    this->q_BN_k_m << 1.0, 0.0, 0.0, 0.0;
    this->q_BN_k_p << 1.0, 0.0, 0.0, 0.0;
    this->q_BN_km1_p << 1.0, 0.0, 0.0, 0.0;

    // Set all angular velocity and attitude error members to zero
    this->omega_BN_B_k_m.setZero();
    this->omega_BN_B_k_p.setZero();
    this->omega_BN_B_km1_p.setZero();
    this->deltaTheta_k_m.setZero();
    this->deltaTheta_k_p.setZero();
    this->deltaTheta_km1_p.setZero();

    // Set commanded torque member to zero
    this->cmdTorqueEigen.setZero();

    // Set magnetic field and sun position vectors to arbitrary unit vectors
    this->sHat_N << 1.0, 0.0, 0.0;
    this->bHat_N << 1.0, 0.0, 0.0;
    this->sHatMeas_B << 1.0, 0.0, 0.0;
    this->bHatMeas_B << 1.0, 0.0, 0.0;

    // Set inertia matrix to identity and propagation timestep to 1 sec - 
    // These must be set using the provided methods.
    this->IHubPntBc_B.setIdentity();
    this->invIHubPntBc_B.setIdentity();
    this->dtPropagation = 1.0;

    // Set Kalman filter jacobians and gain to zero
    this->jacobianFSS.setZero();
    this->jacobianMag.setZero();
    this->jacobianDynamics.setZero();
    this->gainKalman.setZero();

    // Initialize Kalman filter covariance. CAUTION: This is initialized as zero here,
    // in order to have an existing value, but its initial value shall definitely be updated.
    // A method to set an initial prediction for the covariance is provided for that purpose.
    this->covarKalman_k_m.setZero();
    this->covarKalman_k_p.setZero();
    this->covarKalman_km1_p.setZero();
}


MagFSSEKF::~MagFSSEKF(){

}


void MagFSSEKF::Reset(uint64_t current_sim_nanos){
    if(!this->currentSpacecraftStateMsg.isLinked()){
        bskLogger.bskLog(BSK_ERROR, "MagFSSEKF: Failed to link a spacecraft state input message");
    }
    if(!this->dataFSSMsg.isLinked()){
        bskLogger.bskLog(BSK_ERROR, "MagFSSEKF: Failed to link a FSS measurement input message");
    }
    if(!this->dataMagMsg.isLinked()){
        bskLogger.bskLog(BSK_ERROR, "MagFSSEKF: Failed to link a magnetometer measurement input message");
    }
    if(!this->dataMagModelMsg.isLinked()){
        bskLogger.bskLog(BSK_ERROR, "MagFSSEKF: Failed to link a magnetic field model input message");
    }
    if(!this->cmdTorqueMsg.isLinked()){
        bskLogger.bskLog(BSK_ERROR, "MagFSSEKF: Failed to link a torque command input message");
    }
    if(!this->sunDataMsg.isLinked()){
        bskLogger.bskLog(BSK_ERROR, "MagFSSEKF: Failed to link a Sun data input message");
    }

    // TODO: Investigate resetting some of the filter variables here.
}


void MagFSSEKF::UpdateState(uint64_t current_sim_nanos){
    this->readInputMessages();

    // Update the _km1_p variables
    this->updateVariablesTimestep();

    // EKF propagation step
    this->propagateDynamics();
    this->computeDynamicsJacobian();
    this->propagateCovariance();

    // Update the sensor Jacobians
    this->computeMagJacobian();
    this->computeFSSJacobian();

    // Perform the magnetometer measurement update
    this->updateKalmanGain(0);
    this->updateCovariance(0);
    this->updateEstimation(0);
    this->resetErrorState();

    // Perform the FSS measurement update if there is a valid measurement
    if(this->dataFSS.isValid == true){
        this->updateKalmanGain(1);
        this->updateCovariance(1);
        this->updateEstimation(1);
        this->resetErrorState();
    }

    this->writeOutputMessage(current_sim_nanos);
}


void MagFSSEKF::readInputMessages(){
    // Zero the content of message buffers
    this->currentSpacecraftState = this->currentSpacecraftStateMsg.zeroMsgPayload;
    this->dataFSS = this->dataFSSMsg.zeroMsgPayload;
    this->dataMag = this->dataMagMsg.zeroMsgPayload;
    this->dataMagModel = this->dataMagModelMsg.zeroMsgPayload;
    this->cmdTorque = this->cmdTorqueMsg.zeroMsgPayload;
    this->sunData = this->sunDataMsg.zeroMsgPayload;

    // Copy message content from the message to the buffer
    this->currentSpacecraftState = this->currentSpacecraftStateMsg();
    this->dataFSS = this->dataFSSMsg();
    this->dataMag = this->dataMagMsg();
    this->dataMagModel = this->dataMagModelMsg();
    this->cmdTorque = this->cmdTorqueMsg();
    this->sunData = this->sunDataMsg();

    // copy data from messages to eigen vectors, sHat_B, sHatMeas_B and bHat_B, bHatMeas_B, cmdTorqueEigen
    this->bHat_N << this->dataMagModel.magField_N[0], this->dataMagModel.magField_N[1], this->dataMagModel.magField_N[2];
    this->sHat_N << this->sunData.PositionVector[0], this->sunData.PositionVector[1], this->sunData.PositionVector[2];
    this->sHat_N = this->sHat_N / this->sHat_N.norm();
    this->bHatMeas_B << this->dataMag.tam_S[0], this->dataMag.tam_S[1], this->dataMag.tam_S[2];
    this->sHatMeas_B << this->dataFSS.sHat_B[0], this->dataFSS.sHat_B[1], this->dataFSS.sHat_B[2];
    this->cmdTorqueEigen << this->cmdTorque.torqueRequestBody[0], this->cmdTorque.torqueRequestBody[1], this->cmdTorque.torqueRequestBody[2]; 
}


void MagFSSEKF::writeOutputMessage(uint64_t curent_sim_nanos){
    if(this->attDataOutMsg.isLinked()){
        // calculate MRPs from quaternion
        Eigen::Vector3d sigma_BN;
        sigma_BN << this->q_BN_k_p.tail(3)/(1 + this->q_BN_k_p[0]);

        NavAttMsgPayload outMsg;
        outMsg = this->attDataOutMsg.zeroMsgPayload;
        outMsg.timeTag = (double)curent_sim_nanos / SEC2NANO;
        outMsg.sigma_BN[0] = sigma_BN[0];
        outMsg.sigma_BN[1] = sigma_BN[1];
        outMsg.sigma_BN[2] = sigma_BN[2];
        outMsg.omega_BN_B[0] = this->omega_BN_B_k_p[0];
        outMsg.omega_BN_B[1] = this->omega_BN_B_k_p[1];
        outMsg.omega_BN_B[2] = this->omega_BN_B_k_p[2];

        this->attDataOutMsg.write(&outMsg, this->moduleID, curent_sim_nanos);
    }
}


void MagFSSEKF::propagateDynamics(){
    Eigen::Matrix<double, 4, 3> ksi;
    ksi << this->q_BN_km1_p[0], -this->q_BN_km1_p[3], this->q_BN_km1_p[2],
           this->q_BN_km1_p[3], this->q_BN_km1_p[0], -this->q_BN_km1_p[1],
           -this->q_BN_km1_p[2], this->q_BN_km1_p[1], this->q_BN_km1_p[4],
           -this->q_BN_km1_p[1], -this->q_BN_km1_p[2], -this->q_BN_km1_p[3];

    // Integrate attitude dynamics using Euler integration
    this->q_BN_k_m = this->dtPropagation * (0.5*ksi*this->omega_BN_B_km1_p) + this->q_BN_km1_p;

    // TODO: Add angular velocity due to orbital motion somehow
    this->omega_BN_B_k_m = this->dtPropagation * (this->invIHubPntBc_B * (-(this->omega_BN_B_km1_p.cross(this->IHubPntBc_B * this->omega_BN_B_km1_p)) + this->cmdTorqueEigen)) + this->omega_BN_B_km1_p;
}


void MagFSSEKF::computeDynamicsJacobian(){
    // Note that the MEKF propagates the full MEKF state, but uses the
    // covariance of the error state. So the dynamics Jacobian refers to the
    // attitude error dynamics.

    this->jacobianDynamics(0, 0) = 0.0;
    this->jacobianDynamics(0, 1) = this->omega_BN_B_km1_p[2];
    this->jacobianDynamics(0, 2) = -this->omega_BN_B_km1_p[1];
    this->jacobianDynamics(0, 3) = 0.0;
    this->jacobianDynamics(0, 4) = -this->deltaTheta_km1_p[2];
    this->jacobianDynamics(0, 5) = this->deltaTheta_km1_p[1];

    this->jacobianDynamics(1, 0) = -this->omega_BN_B_km1_p[2];
    this->jacobianDynamics(1, 1) = 0.0;
    this->jacobianDynamics(1, 2) = -this->omega_BN_B_km1_p[0];
    this->jacobianDynamics(1, 3) = this->deltaTheta_km1_p[2];
    this->jacobianDynamics(1, 4) = 0.0;
    this->jacobianDynamics(1, 5) = -this->deltaTheta_km1_p[0];

    this->jacobianDynamics(2, 0) = this->omega_BN_B_km1_p[1];
    this->jacobianDynamics(2, 1) = -this->omega_BN_B_km1_p[0];
    this->jacobianDynamics(2, 2) = 0.0;
    this->jacobianDynamics(2, 3) = -this->deltaTheta_km1_p[1];
    this->jacobianDynamics(2, 4) = this->deltaTheta_km1_p[0];
    this->jacobianDynamics(2, 5) = 0.0;

    this->jacobianDynamics(3, 0) = 0.0;
    this->jacobianDynamics(3, 1) = 0.0;
    this->jacobianDynamics(3, 2) = 0.0;
    this->jacobianDynamics(3, 3) = 0.0;
    this->jacobianDynamics(3, 4) = this->omega_BN_B_km1_p[2] * (this->IHubPntBc_B(1, 1) - this->IHubPntBc_B(2, 2)) / this->IHubPntBc_B(0, 0);
    this->jacobianDynamics(3, 5) = this->omega_BN_B_km1_p[1] * (this->IHubPntBc_B(1, 1) - this->IHubPntBc_B(2, 2)) / this->IHubPntBc_B(0, 0);

    this->jacobianDynamics(4, 0) = 0.0;
    this->jacobianDynamics(4, 1) = 0.0;
    this->jacobianDynamics(4, 2) = 0.0;
    this->jacobianDynamics(4, 3) = this->omega_BN_B_km1_p[2] * (-this->IHubPntBc_B(0, 0) + this->IHubPntBc_B(2, 2)) / this->IHubPntBc_B(1, 1);
    this->jacobianDynamics(4, 4) = 0.0;
    this->jacobianDynamics(4, 5) = this->omega_BN_B_km1_p[0] * (-this->IHubPntBc_B(1, 1) + this->IHubPntBc_B(2, 2)) / this->IHubPntBc_B(1, 1);

    this->jacobianDynamics(5, 0) = 0.0;
    this->jacobianDynamics(5, 1) = 0.0;
    this->jacobianDynamics(5, 2) = 0.0;
    this->jacobianDynamics(5, 3) = this->omega_BN_B_km1_p[1] * (this->IHubPntBc_B(0, 0) - this->IHubPntBc_B(1, 1)) / this->IHubPntBc_B(2, 2);
    this->jacobianDynamics(5, 4) = this->omega_BN_B_km1_p[1] * (this->IHubPntBc_B(0, 0) - this->IHubPntBc_B(1, 1)) / this->IHubPntBc_B(2, 2);
    this->jacobianDynamics(5, 5) = 0.0;
}


void MagFSSEKF::computeFSSJacobian(){
    Eigen::Matrix3d sHat_N_skew;
    sHat_N_skew <<  0.0, -this->sHat_N.z(), this->sHat_N.y(),
                    this->sHat_N.z(), 0.0, -this->sHat_N.x(),
                    -this->sHat_N.y(), this->sHat_N.x(), 0.0;

    this->jacobianFSS.setZero();
    this->jacobianFSS.block<3, 3>(0, 0) = quatToDCM(this->q_BN_k_m)*sHat_N_skew;
}


void MagFSSEKF::computeMagJacobian(){
    Eigen::Matrix3d bHat_N_skew;
    bHat_N_skew <<  0.0, -this->bHat_N.z(), this->bHat_N.y(),
                    this->bHat_N.z(), 0.0, -this->bHat_N.x(),
                    -this->bHat_N.y(), this->bHat_N.x(), 0.0;

    this->jacobianMag.setZero();
    this->jacobianMag.block<3, 3>(0, 0) = quatToDCM(this->q_BN_k_m)*bHat_N_skew;
}


Eigen::Vector<double, MEASUREMENT_VECTOR_LEN> MagFSSEKF::sensorModelMag(){
    return quatToDCM(this->q_BN_k_m) * this->bHat_N;
}


Eigen::Vector<double, MEASUREMENT_VECTOR_LEN> MagFSSEKF::sensorModelFSS(){
    return quatToDCM(this->q_BN_k_m) * this->sHat_N;
}


void MagFSSEKF::propagateCovariance(){
    this->covarKalman_k_m = this->jacobianDynamics * this->covarKalman_km1_p * this->jacobianDynamics.transpose() + covarState;
}


void MagFSSEKF::updateKalmanGain(int sensor){
    // sensor == 0 if a magnetometer measurement was provided
    if(sensor == 0){
        gainKalman = this->covarKalman_k_m * this->jacobianMag.transpose() * (
            this->jacobianMag * this->covarKalman_k_m * this->jacobianMag.transpose() + this->covarMag
        ).inverse();
    }

    // sensor == 1 if a FSS measurement was provided
    if(sensor == 1){
        gainKalman = this->covarKalman_k_m * this->jacobianFSS.transpose() * (
            this->jacobianFSS * this->covarKalman_k_m * this->jacobianFSS.transpose() + this->covarFSS
        ).inverse();
    }
}

void MagFSSEKF::updateCovariance(int sensor){
    // sensor == 0 if a magnetometer measurement was provided
    if(sensor == 0){
        this->covarKalman_k_p = (
            Eigen::MatrixXd::Identity(STATE_VECTOR_LEN, STATE_VECTOR_LEN) - this->gainKalman * this->jacobianMag
        ) * this->covarKalman_k_m;
    }

    // sensor == 1 if a FSS measurement was provided
    if(sensor == 1){
        this->covarKalman_k_p = (
            Eigen::MatrixXd::Identity(STATE_VECTOR_LEN, STATE_VECTOR_LEN) - this->gainKalman * this->jacobianFSS
        ) * this->covarKalman_k_m;
    }
}

void MagFSSEKF::updateEstimation(int sensor){
    // Considering that the updateKalmanGain function has been called beforehand

    // Compile the deltaTheta and omega vectors into a single vector
    Eigen::Vector<double, 6> x_k_m;
    x_k_m << deltaTheta_k_m, omega_BN_B_k_m;
    Eigen::Vector<double, 6> x_k_p;

    // sensor == 0 if a magnetometer measurement was provided
    if(sensor == 0){
        x_k_p = x_k_m + this->gainKalman*(this->bHatMeas_B - this->sensorModelMag());
    }

    // sensor == 1 if a FSS measurement was provided
    if(sensor == 1){
        x_k_p = x_k_m + this->gainKalman*(this->sHatMeas_B - this->sensorModelFSS());
    }

    // split x_k_p into deltaTheta and omega
    deltaTheta_k_p << x_k_p.head(3);
    omega_BN_B_k_p << x_k_p.tail(3);
}


void MagFSSEKF::resetErrorState(){
    Eigen::Matrix<double, 4, 3> ksi;
    ksi << this->q_BN_k_m[0], -this->q_BN_k_m[3], this->q_BN_k_m[2],
           this->q_BN_k_m[3], this->q_BN_k_m[0], -this->q_BN_k_m[1],
           -this->q_BN_k_m[2], this->q_BN_k_m[1], this->q_BN_k_m[4],
           -this->q_BN_k_m[1], -this->q_BN_k_m[2], -this->q_BN_k_m[3];

    // Transfer information to the full quaternion
    // TODO: CHECK WHETHER THE REFERENCE FRAMES ARE OK WITH THIS RESET FORMULA
    this->q_BN_k_p = this->q_BN_k_m + 0.5*ksi*this->deltaTheta_k_p;
    this->q_BN_k_p = this->q_BN_k_p / this->q_BN_k_p.norm(); 

    // Reset error variables
    this->deltaTheta_k_m << 0.0, 0.0, 0.0;
}


void MagFSSEKF::updateVariablesTimestep(){
    this->q_BN_km1_p = this->q_BN_k_p;
    this->omega_BN_B_km1_p = this->omega_BN_B_k_p;
    this->deltaTheta_km1_p = this->deltaTheta_k_p;
    this->covarKalman_km1_p = this->covarKalman_k_p;
}


void MagFSSEKF::setCovarMag(Eigen::Matrix3d _covarMag){
    this->covarMag = _covarMag;
}


void MagFSSEKF::setCovarFSS(Eigen::Matrix3d _covarFSS){
    this->covarFSS = _covarFSS;
}

/**
 * @brief 
 * 
 * Matrices higher than dimensions 3x3 need to be passed as Eigen::MatrixXd
 * so that there are no problems with SWIG.
 * @param _covarState 
 */
void MagFSSEKF::setCovarState(Eigen::MatrixXd _covarState){
    this->covarState = _covarState;
}

void MagFSSEKF::setInitQuaternion(Eigen::VectorXd _q_BN_km1_p){
    this->q_BN_km1_p = _q_BN_km1_p;
}

void MagFSSEKF::setInitOmega(Eigen::Vector3d _omega_BN_B_km1_p){
    this->omega_BN_B_km1_p = _omega_BN_B_km1_p;
}

void MagFSSEKF::setInitCovarKalman(Eigen::MatrixXd _covarKalman_km1_p){
    this->covarKalman_km1_p = _covarKalman_km1_p;
}

void MagFSSEKF::setIHub(Eigen::Matrix3d _IHubPntBc_B){
    this->IHubPntBc_B = _IHubPntBc_B;
    this->invIHubPntBc_B = _IHubPntBc_B.inverse();
}

void MagFSSEKF::setDtPropagation(double _dtPropagation){
    this->dtPropagation = _dtPropagation;
}


Eigen::Matrix3d deltaThetaToDCM(Eigen::Vector3d deltaTheta){
    Eigen::Matrix3d deltaThetaSkew;
    deltaThetaSkew <<  0,    -deltaTheta.z(),  deltaTheta.y(),
                       deltaTheta.z(),  0,    -deltaTheta.x(),
                       -deltaTheta.y(),  deltaTheta.x(),  0;

    return Eigen::Matrix3d::Identity(3, 3) - deltaThetaSkew + 0.5 * deltaThetaSkew * deltaThetaSkew;
}


Eigen::Matrix3d quatToDCM(Eigen::Vector4d q){
    Eigen::Matrix3d qSkew;
    qSkew << 0.0, -q[3], q[2],
             q[3], 0.0, -q[1],
             -q[2], q[1], 0.0;
    
    return (2*q[0]*q[0] -1)*Eigen::MatrixXd::Identity(3, 3) + 2*q[0]*qSkew + 2*q.tail(3)*(q.tail(3).transpose());
}


Eigen::Vector4d quatMultiply(Eigen::Vector4d q1, Eigen::Vector4d q2){
    // Considering a scalar-first notation for the quaternions
    // This is the "cross" multiplication of quaternions

    Eigen::Vector4d out;
    Eigen::Vector3d q1_13 = q1.tail(3);
    Eigen::Vector3d q2_13 = q2.tail(3);

    out << q1[0]*q2[0] - (q1.tail(3)).dot(q2.tail(3)),
           q2[0]*q1.tail(3) + q1[0]*q2.tail(3) - (q1_13).cross(q2_13);

    return out;
}
