%module magFSSEKF
%{
   #include "magFSSEKF.h"
%}

%include "swig_conly_data.i"
%include "swig_eigen.i"
%include "std_vector.i"
%include "std_string.i"

%include "sys_model.i"
%include "magFSSEKF.h"

%include "msgPayloadDefCpp/FSSRawDataMsgPayload.h"
%include "architecture/msgPayloadDefC/SCStatesMsgPayload.h"
%include "architecture/msgPayloadDefC/TAMSensorMsgPayload.h"
%include "architecture/msgPayloadDefC/MagneticFieldMsgPayload.h"
%include "architecture/msgPayloadDefC/CmdTorqueBodyMsgPayload.h"
%include "architecture/msgPayloadDefC/NavAttMsgPayload.h"
%include "architecture/msgPayloadDefC/SpicePlanetStateMsgPayload.h"

struct FSSRawDataMsg_C;
struct SCStatesMsg_C;
struct TAMSensorMsgPayload_C;
struct MagneticFieldMsg_C;
struct CmdTorqueBodyMsg_C;
struct NavAttMsgPayloadMsg_C;
struct SpicePlanetStateMsg_C;

%pythoncode %{
import sys
protectAllClasses(sys.modules[__name__])
%}
