import sympy as sy
from sympy.physics.vector import *
from sympy.matrices.expressions import hadamard_product


def quat_multiply(q1: sy.Matrix, q2: sy.Matrix):
    # This is the "cross" quaternion multiplication q1 x q2
    # Considering a scalar-last quaternion notation

    # v = hadamard_product(q2[0], q1[1:4]) + hadamard_product(q1[0], q2[1:4]) - (q1[1:4].cross(q2[1:4]))
    v = sy.Matrix([q2[0]*q1[1], q2[0]*q1[2], q2[0]*q1[2]]) + sy.Matrix([q1[0]*q2[1], q1[0]*q2[2], q1[0]*q2[2]]) - (sy.Matrix(q1[1:4]).cross(sy.Matrix(q2[1:4])))
    # v = q2[0]*q1[1:4] + q1[0]*q2[1:4] - (q1[1:4].cross(q2[1:4]))
    s = q1[0]*q2[0] - (sy.Matrix(q1[1:4]).dot(sy.Matrix(q2[1:4])))

    return sy.Matrix.vstack(sy.Matrix([s]), v)


def quat_to_dcm(q:sy.Matrix):
    """
    Computes the Direction Cosine Matrix (DCM) from a scalar-first quaternion.
    
    Args:
        q (Matrix): A 4x1 SymPy Matrix representing the quaternion [q0, q1, q2, q3] (scalar-first).
    
    Returns:
        Matrix: A 3x3 SymPy Matrix representing the corresponding rotation matrix (DCM).
    """
    q0 = q[0]  # Scalar part
    qv = sy.Matrix(q[1:4])  # Vector part (3x1)

    # Compute the skew-symmetric matrix of qv
    qv_skew = sy.Matrix([
        [0, -qv[2], qv[1]],
        [qv[2], 0, -qv[0]],
        [-qv[1], qv[0], 0]
    ])

    # Compute the DCM
    DCM = (2 * q0**2 - 1) * sy.eye(3) + 2 * q0 * qv_skew + 2 * (qv * qv.transpose())

    return DCM


def quat_from_delta_theta(dth:sy.Matrix):
    out = sy.Matrix.vstack(sy.Matrix([1]), dth/2)
    return out


# NOTE: Sympy creates column vectors by default
# REFERENCES: 
# [1] F. L. Markley and J. L. Crassidis, Fundamentals of Spacecraft Attitude Determination and Control,
#     New York: Microcosm Press and Springer, 2014.

# quaternion error
dth0, dth1, dth2 = sy.symbols('dth0:3')
dth = sy.Matrix([dth0, dth1, dth2])

# previous quaternion prediction
qm0, qm1, qm2, qm3 = sy.symbols('qm0:4')
qm = sy.Matrix([qm0, qm1, qm2, qm3])

# angular velocity
w0, w1, w2 = sy.symbols('w0:3')
w = sy.Matrix([w0, w1, w2])

# torque
u0, u1, u2 = sy.symbols('u0:3')
u = sy.Matrix([u0, u1, u2])

# moment of inertia
moi = sy.MatrixSymbol('J', 3, 3)
moi = sy.Matrix(moi)
moi[0, 1] = 0
moi[0, 2] = 0
moi[1, 0] = 0
moi[1, 2] = 0
moi[2, 0] = 0
moi[2, 1] = 0

moi_inv = moi.inv()

# measured vector
mv0, mv1, mv2 = sy.symbols('mv0:3')
mv = sy.Matrix([mv0, mv1, mv2])

# Calculate the dynamics model and its jacobian
dynamics_model = sy.Matrix.vstack(-w.cross(dth), moi_inv*(-(w.cross(moi*w)) + u))
dynamics_jacobian = dynamics_model.jacobian([dth, w])
print("====================== DYNAMICS MODEL JACOBIAN ====================")
sy.pretty_print(sy.simplify(dynamics_jacobian))
