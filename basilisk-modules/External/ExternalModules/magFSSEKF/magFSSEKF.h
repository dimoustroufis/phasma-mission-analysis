#ifndef MAGFSSEKF_H
#define MAGFSSEKF_H

#include "architecture/_GeneralModuleFiles/sys_model.h"
#include "architecture/utilities/bskLogging.h"
#include "architecture/messaging/messaging.h"
#include "msgPayloadDefCpp/FSSRawDataMsgPayload.h"
#include "architecture/msgPayloadDefC/SCStatesMsgPayload.h"
#include "architecture/msgPayloadDefC/TAMSensorMsgPayload.h"
#include "architecture/msgPayloadDefC/MagneticFieldMsgPayload.h"
#include "architecture/msgPayloadDefC/CmdTorqueBodyMsgPayload.h"
#include "architecture/msgPayloadDefC/NavAttMsgPayload.h"
#include "architecture/msgPayloadDefC/SpicePlanetStateMsgPayload.h"
#include "architecture/utilities/macroDefinitions.h"
#include <eigen3/Eigen/Core>
#include <random>

#define STATE_VECTOR_LEN 6
#define MEASUREMENT_VECTOR_LEN 3

class MagFSSEKF: public SysModel{
    private:
        // State and sensor covariance matrices
        Eigen::Matrix3d covarMag; /* Magnetometer covariance matrix (sensor noise) */
        Eigen::Matrix3d covarFSS; /* FSS covariance matrix (sensor noise) */
        Eigen::Matrix<double, 6, 6> covarState; /* State covariance matrix (process noise) */

        // Constant variables
        Eigen::Matrix3d IHubPntBc_B; /* Inertia matrix around the center of the body frame, expressed in the body frame */
        Eigen::Matrix3d invIHubPntBc_B; /* Inverse inertia matrix around the center of the body frame, expressed in the body frame */
        double dtPropagation; /* Timestep at which dynamics propagation is performed. This is the timestep at which the filter is run */

        // Operational Kalman filter variables
            // "true" quaternion and angular velocity - scalar FIRST convention for the quaternions
        Eigen::Vector4d q_BN_k_m; /* Full quaternion from the inertial to the body frame, at step k */
        Eigen::Vector4d q_BN_k_p; /* Full quaternion from the inertial to the body frame, at step k */
        Eigen::Vector4d q_BN_km1_p; /* Full quaternion from the inertial to the body frame, at step k-1 */
        Eigen::Vector3d omega_BN_B_k_m; /* Angular velocity of the Body wrt the inertial frame, expressed in the body frame, at step k, before update */
        Eigen::Vector3d omega_BN_B_k_p; /* Angular velocity of the Body wrt the inertial frame, expressed in the body frame, at step k, after update */
        Eigen::Vector3d omega_BN_B_km1_p; /* Angular velocity of the Body wrt the inertial frame, expressed in the body frame, after all updates of step k-1 */
            // quaternion and angular velocity errors
        Eigen::Vector3d deltaTheta_k_m; /* Error quaternion state at step k, before update */
        Eigen::Vector3d deltaTheta_k_p; /* Error quaternion state at step k, after update */
        Eigen::Vector3d deltaTheta_km1_p; /* Error quaternion state at step k-1, after all updates of the k-1 step */
            // commanded torque
        Eigen::Vector3d cmdTorqueEigen; /* Eigen vector for the commanded torque */
        Eigen::Vector3d sHat_N; /* Eigen vector for the model sun heading vector */
        Eigen::Vector3d bHat_N; /* Eigen vector for the model magnetic field vector */
        Eigen::Vector3d sHatMeas_B; /* Eigen vector for the measured sun heading vector */
        Eigen::Vector3d bHatMeas_B; /* Eigen vector for the measured magnetic field vector */
            // jacobians and covariances
        Eigen::Matrix<double, 3, 6> jacobianFSS;
        Eigen::Matrix<double, 3, 6> jacobianMag;
        Eigen::Matrix<double, 6, 6> jacobianDynamics;
        Eigen::Matrix<double, 6, 6> covarKalman_km1_p;
        Eigen::Matrix<double, 6, 6> covarKalman_k_p;
        Eigen::Matrix<double, 6, 6> covarKalman_k_m;
        Eigen::Matrix<double, 6, 3> gainKalman;

        // Variables to store input and output messages
        SCStatesMsgPayload currentSpacecraftState; /* Current spacecraft state (used for position) */
        FSSRawDataMsgPayload dataFSS; /* Fine Sun Sensor input */
        TAMSensorMsgPayload dataMag; /* Magnetometer input */
        MagneticFieldMsgPayload  dataMagModel; /* Magnetic field model input */
        CmdTorqueBodyMsgPayload cmdTorque; /* Commanded torque in the body frame */
        SpicePlanetStateMsgPayload sunData; /* Sun ephemeris data */


    public:
        MagFSSEKF();
        ~MagFSSEKF();

        // Basilisk simulation functions
        void Reset(uint64_t current_sim_nanos);
        void UpdateState(uint64_t current_sim_nanos);
        void readInputMessages();
        void writeOutputMessage(uint64_t current_sim_nanos);

        // Main operational functions
        void propagateDynamics(); /* Calculates x_k_m */
        void propagateCovariance(); /* Calculates P_k_m */
        void updateKalmanGain(int sensor); /* Calculate Kk */
        void updateCovariance(int sensor); /* Calculate P_k_p */
        void updateEstimation(int sensor); /* Calculate x_k_p */
        void resetErrorState(); /* Transfer info from error state to full state */

        // Auxiliary operational functions
        void computeFSSJacobian(); /* Computes the Jacobian of the FSS model. Magnetometer and FSS measurement models are the same */
        void computeMagJacobian(); /* Computes the Jacobian of the mag model. Magnetometer and FSS measurement models are the same */
        void computeDynamicsJacobian(); /* Computes the Jacobian of the dynamics model */
        Eigen::Vector<double, MEASUREMENT_VECTOR_LEN> sensorModelMag(); /* Compute the magnetometer sensor model */
        Eigen::Vector<double, MEASUREMENT_VECTOR_LEN> sensorModelFSS(); /* Compute the FSS sensor model */
        void updateVariablesTimestep(); /* Use _k_p variables as the _km1_p variables */

        // Setter functions
        void setCovarMag(Eigen::Matrix3d _covarMag); /* Sets the magnetometer covariance matrix (sensor noise) */
        void setCovarFSS(Eigen::Matrix3d _covarFSS); /* Sets the FSS covariance matrix (sensor noise) */
        void setCovarState(Eigen::MatrixXd _covarState); /* Sets the state covariance matrix (process noise) */
        void setInitQuaternion(Eigen::VectorXd _q_BN_km1_p); /* Sets the initial quaternion prediction */
        void setInitOmega(Eigen::Vector3d _omega_BN_B_km1_p); /* Sets the initial angular velocity prediction */
        void setInitCovarKalman(Eigen::MatrixXd _covarKalman_km1_p); /* Sets the initial prediction for the filter covariance */
        void setIHub(Eigen::Matrix3d _IHubPntBc_B); /* Sets the matrix of inertia and its inverse */
        void setDtPropagation(double _dtPropagation); /* Sets the propagation timestep */

        // Messaging
        Message<NavAttMsgPayload> attDataOutMsg; /* Attitude data output message */
        ReadFunctor<SCStatesMsgPayload> currentSpacecraftStateMsg; /* Input message for the current spacecraft state */
        ReadFunctor<FSSRawDataMsgPayload> dataFSSMsg; /* Input message for FSS measurement data */
        ReadFunctor<TAMSensorMsgPayload> dataMagMsg; /* Input message for magnetometer measurement data */
        ReadFunctor<MagneticFieldMsgPayload> dataMagModelMsg; /* Input message for magnetic field model data */
        ReadFunctor<CmdTorqueBodyMsgPayload> cmdTorqueMsg; /* Input message for torque command */
        ReadFunctor<SpicePlanetStateMsgPayload> sunDataMsg; /* Input message for sun data */
        BSKLogger bskLogger; /* BSK Logging */


};

Eigen::Matrix3d deltaThetaToDCM(Eigen::Vector3d deltaTheta); /* Computes the DCM corresponding to an attitude error vector */
Eigen::Matrix3d quatToDCM(Eigen::Vector4d q); /* Computes the DCM corresponding to a quaternion */
Eigen::Vector4d quatMultiply(Eigen::Vector4d q1, Eigen::Vector4d q2); /* Conducts the "cross" quaternion multiplication */

#endif
