import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import phasmalibma.utilities

# simulation input-output file directory
simul_dir = "adcs_pointing_power/config_42/"

# read the timestep from the Inp_sim file
timestep = phasmalibma.utilities.find_42_timestep(simul_dir)

# Define colors for plotting
colors = ["dodgerblue", "forestgreen", "firebrick", "darkmagenta"]

# Read moment-current curves for various torques
h_i_colnames = ["momentum", "current"]
curves_dir = "adcs_pointing_power/cw0017_8V_current_curves/"
curve_steady = pd.read_csv(
    curves_dir + "steady_state.csv", names=h_i_colnames, dtype="float64"
)
curve_steady = np.array(curve_steady)
curve_005 = pd.read_csv(
    curves_dir + "0_05_mNm.csv", names=h_i_colnames, dtype="float64"
)
curve_005 = np.array(curve_005)
curve_013 = pd.read_csv(
    curves_dir + "0_13_mNm.csv", names=h_i_colnames, dtype="float64"
)
curve_013 = np.array(curve_013)
curve_023 = pd.read_csv(
    curves_dir + "0_23_mNm.csv", names=h_i_colnames, dtype="float64"
)
curve_023 = np.array(curve_023)

# Read actuator moment results from 42
h_rw_path = simul_dir + "Hwhl.42"
rw_colnames = ["Wheel 0", "Wheel 1", "Wheel 2"]
Hwhl = pd.read_csv(
    h_rw_path, delimiter=" ", header=None, usecols=[0, 1, 2], names=rw_colnames
)  # usecols is needed because 42 adds a white space character at the end and python reads an extra collumn
Hwhl = np.array(Hwhl)
Hwhl *= 1000  # convert to mNm because these are the provided units in the curves

# construct time array
times = np.zeros(Hwhl.shape[0])
for i in range(1, Hwhl.shape[0]):
    times[i] = times[i - 1] + timestep

# Calculate torques as the derivative of the angular momentum
# normally, torque arrays would have 1 less element than the ang mom arrays,
# but to keep sizes equal the last torue element is set equal to the one before it
Twhl = np.gradient(Hwhl, times, axis=0)  # mNm


# Calculate reaction wheel POWER for every timepoint for every torque level
# the results are gathered in 3 n x 6 np.arrays
# (1 for each wheel with cols: [time, steady, 005, 013, 023, interp])
# The interp column contains the power value for the torque calculated above
# UNITS: Voltage: V, current in curves: mA, power: mW
rw_voltage = 8  # [V]
wheel_0_power = np.zeros([Hwhl.shape[0], 6])
wheel_1_power = np.zeros([Hwhl.shape[0], 6])
wheel_2_power = np.zeros([Hwhl.shape[0], 6])

wheel_0_power[:, 0] = times
wheel_1_power[:, 0] = times
wheel_2_power[:, 0] = times

# Interpolate momentum-current curves to get power at the various torque levels
wheel_0_power[:, 1] = rw_voltage * np.interp(
    Hwhl[:, 0], curve_steady[:, 0], curve_steady[:, 1]
)
wheel_1_power[:, 1] = rw_voltage * np.interp(
    Hwhl[:, 1], curve_steady[:, 0], curve_steady[:, 1]
)
wheel_2_power[:, 1] = rw_voltage * np.interp(
    Hwhl[:, 2], curve_steady[:, 0], curve_steady[:, 1]
)

wheel_0_power[:, 2] = rw_voltage * np.interp(
    Hwhl[:, 0], curve_005[:, 0], curve_005[:, 1]
)
wheel_1_power[:, 2] = rw_voltage * np.interp(
    Hwhl[:, 1], curve_005[:, 0], curve_005[:, 1]
)
wheel_2_power[:, 2] = rw_voltage * np.interp(
    Hwhl[:, 2], curve_005[:, 0], curve_005[:, 1]
)

wheel_0_power[:, 3] = rw_voltage * np.interp(
    Hwhl[:, 0], curve_013[:, 0], curve_013[:, 1]
)
wheel_1_power[:, 3] = rw_voltage * np.interp(
    Hwhl[:, 1], curve_013[:, 0], curve_013[:, 1]
)
wheel_2_power[:, 3] = rw_voltage * np.interp(
    Hwhl[:, 2], curve_013[:, 0], curve_013[:, 1]
)

wheel_0_power[:, 4] = rw_voltage * np.interp(
    Hwhl[:, 0], curve_023[:, 0], curve_023[:, 1]
)
wheel_1_power[:, 4] = rw_voltage * np.interp(
    Hwhl[:, 1], curve_023[:, 0], curve_023[:, 1]
)
wheel_2_power[:, 4] = rw_voltage * np.interp(
    Hwhl[:, 2], curve_023[:, 0], curve_023[:, 1]
)

# Interpolate between the torque levels to get actual power drawn in the simulation scenario
torque_levels = np.array(
    [0, 0.05, 0.13, 0.23]
)  # [mNm] 0.0 refers to the "steady state" curve
for i in range(Twhl.shape[0]):
    # using absolute torque for interpolation because torque levels have been expressed as absolute values
    wheel_0_power[i, 5] = np.interp(
        abs(Twhl[i, 0]), torque_levels, wheel_0_power[i, 1:5]
    )
    wheel_1_power[i, 5] = np.interp(
        abs(Twhl[i, 1]), torque_levels, wheel_1_power[i, 1:5]
    )
    wheel_2_power[i, 5] = np.interp(
        abs(Twhl[i, 2]), torque_levels, wheel_2_power[i, 1:5]
    )

# Average reaction wheel power over the simulation time
wheel_0_avg_power = np.mean(wheel_0_power[:, 5], axis=0)
wheel_1_avg_power = np.mean(wheel_1_power[:, 5], axis=0)
wheel_2_avg_power = np.mean(wheel_2_power[:, 5], axis=0)

print("Wheel 0 average power over simulation duration [mW] : ", wheel_0_avg_power)
print("Wheel 1 average power over simulation duration [mW] : ", wheel_1_avg_power)
print("Wheel 2 average power over simulation duration [mW]: ", wheel_2_avg_power)


fig, ax = plt.subplots()
ax.plot(times, Twhl[:, 0], c=colors[0])
ax.plot(times, Twhl[:, 1], c=colors[1])
ax.plot(times, Twhl[:, 2], c=colors[2])
ax.set_ylabel("Torque [mNm]")
ax.set_xlabel("Time [s]")
ax.set_title("Numerically computed (dH/dt) RW torque")
ax.grid(linestyle="--", alpha=0.7)

fig1, ax1 = plt.subplots()
(l1,) = ax1.plot(times, wheel_0_power[:, 5], label="RW 0 pwr", c=colors[0])
(l2,) = ax1.plot(times, wheel_1_power[:, 5], label="RW 1 pwr", c=colors[1])
(l3,) = ax1.plot(times, wheel_2_power[:, 5], label="RW 2 pwr", c=colors[2])
handles = [l1, l2, l3]

ax1.grid()
ax1.set_title("Reaction wheel power")
ax1.set_ylabel("Power [mW]")
ax1.set_xlabel("Time [sec]")
ax1.legend(handles=handles)

plt.show()
